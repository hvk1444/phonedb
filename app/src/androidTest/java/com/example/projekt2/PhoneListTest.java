package com.example.projekt2;


import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.projekt2.activity.MainActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4.class)

public class PhoneListTest {

    @Rule
    public ActivityScenarioRule<MainActivity> activityRule
            = new ActivityScenarioRule<>(MainActivity.class);

    @Test
    public void a_addRecord()
    {

        onView(withId(R.id.addButton))
                .perform(click());
        onView(withId(R.id.insertManufacturerInput))
                .perform(typeText("Test"));
        onView(withId(R.id.insertModelInput))
                .perform(typeText("Tester"));
        onView(withId(R.id.insertVersionInput))
                .perform(typeText("99"));
        onView(withId(R.id.insertWebsiteInput))
                .perform(typeText("https://junit.org/junit4/"));
        onView(withId(R.id.saveButton))
                .perform(click());
        onView(withId(R.id.phoneList))
                .perform(RecyclerViewActions.scrollTo(
                        hasDescendant(withText("Tester"))
                ));
    }

    @Test
    public void b_deleteRecord()
    {

        onView(withId(R.id.phoneList))
                .perform(RecyclerViewActions.actionOnItem(
                        hasDescendant(withText("Tester")), swipeLeft()
                ));
        onView(withText("Tester"))
                .check(doesNotExist());
    }
}
