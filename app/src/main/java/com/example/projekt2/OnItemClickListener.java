package com.example.projekt2;

import android.view.View;

public interface OnItemClickListener {
    void onClick(View v, int position);
}