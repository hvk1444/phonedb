package com.example.projekt2;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "phones")
public class Phone {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "phone_id")
    private long phone_id;

    @NonNull
    @ColumnInfo(name = "manufacturer")
    private String manufacturer;

    @NonNull
    @ColumnInfo(name = "model")
    private String model;

    @NonNull
    @ColumnInfo(name = "system")
    private Integer system;

    @NonNull
    @ColumnInfo(name = "website")
    private String website;

    /*
    @Ignore
    public Phone(@NonNull String manufacturer, @NonNull String model) {
        this.manufacturer = manufacturer;
        this.model = model;
    }
     */

    //full version of constructor
    public Phone(@NonNull String manufacturer, @NonNull String model, @NonNull Integer system, @NonNull String website) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.system = system;
        this.website = website;
    }

    //constructor for updates (when phone_id is known beforehand)
    @Ignore
    public Phone(long phone_id, @NonNull String manufacturer, @NonNull String model, @NonNull Integer system, @NonNull String website) {
        this.phone_id = phone_id;
        this.manufacturer = manufacturer;
        this.model = model;
        this.system = system;
        this.website = website;
    }

    public long getPhone_id() {
        return phone_id;
    }

    public void setPhone_id(long phone_id) {
        this.phone_id = phone_id;
    }

    @NonNull
    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(@NonNull String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @NonNull
    public String getModel() {
        return model;
    }

    public void setModel(@NonNull String model) {
        this.model = model;
    }

    public Integer getSystem() {
        return system;
    }

    public void setSystem(Integer system) {
        this.system = system;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @NonNull
    @Override
    public String toString() {
        return manufacturer + " " + model;
    }
}
