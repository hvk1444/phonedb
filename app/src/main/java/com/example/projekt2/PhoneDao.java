package com.example.projekt2;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface PhoneDao {

    //this method inserts a phone into DB
    //in case of data conflict the operation is aborted
    @Insert(onConflict = OnConflictStrategy.ABORT)
    void insert(Phone phone);

    //this method deletes a phone from DB
    @Delete
    void delete(Phone phone);

    //this method updates a phone in DB
    @Update
    void update(Phone phone);

    //this custom query deletes all records from the table
    @Query("DELETE FROM phones")
    void deleteAll();

    //this custom query returns all records form the table
    //inside a LiveData object
    //this allows the observer to be notified about data change
    //this query is run in a separate thread
    @Query("SELECT * FROM phones ORDER BY model ASC")
    LiveData<List<Phone>> getAlphabetizedPhones();

}
