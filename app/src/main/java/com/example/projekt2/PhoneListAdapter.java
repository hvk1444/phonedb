package com.example.projekt2;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class PhoneListAdapter extends RecyclerView.Adapter<PhoneListAdapter.PhoneViewHolder> {

    private List<Phone> mPhoneList;
    private final LayoutInflater mInflater;
    private OnItemClickListener mOnItemClickListener;

    public PhoneListAdapter(Context context)
    {
        mInflater = LayoutInflater.from(context);
        this.mPhoneList = null;
        try
        {
            mOnItemClickListener = (OnItemClickListener) context;
        } catch (ClassCastException e)
        {
            e.printStackTrace();
        }
    }

    //creates main layout element and a holder for each row
    @NonNull
    @Override
    public PhoneViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //creating row layout according to XML
        View row = mInflater.inflate(R.layout.phone_row, null);
        //returning new holder
        return new PhoneViewHolder(row);
    }

    //sets values for TextViews in each row
    @Override
    public void onBindViewHolder(@NonNull PhoneViewHolder holder, int position) {
        holder.getModelLabel().setText(mPhoneList.get(position).getModel());
        holder.getManufacturerLabel().setText(mPhoneList.get(position).getManufacturer());
    }

    @Override
    public int getItemCount() {
        if(mPhoneList != null)
            return mPhoneList.size();
        return 0;
    }

    public void setPhoneList(List<Phone> phoneList) {
        this.mPhoneList = phoneList;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    //this method returns a phone object from the adapter's list
    //it can be used with view holder's getAdapterPosition() to get phone from the proper index of the list
    public Phone getPhone(int position)
    {
        return mPhoneList.get(position);
    }

    //data to be displayed on the list is going to change
    //therefore this method can be used to update data in the adapter
    //which will then be updated in the RecyclerView
    public class PhoneViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView manufacturerLabel, modelLabel;

        public PhoneViewHolder(@NonNull View itemView) {
            super(itemView);
            manufacturerLabel = itemView.findViewById(R.id.manufacturerLabel);
            modelLabel = itemView.findViewById(R.id.modelLabel);
            itemView.setOnClickListener(this);
        }

        public TextView getManufacturerLabel() {
            return manufacturerLabel;
        }

        public TextView getModelLabel() {
            return modelLabel;
        }

        @Override
        public void onClick(View v) {
            Log.println(Log.INFO, "PhoneViewHolder","PhoneList click received: position " + getAdapterPosition());
            if(mOnItemClickListener != null)
                mOnItemClickListener.onClick(v, getAdapterPosition());
        }
    }

}
