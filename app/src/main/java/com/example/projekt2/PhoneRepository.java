package com.example.projekt2;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

public class PhoneRepository {

    private PhoneDao mPhoneDao;
    private LiveData<List<Phone>> mAllPhones;

    PhoneRepository(Application application)
    {
        PhoneRoomDatabase phoneRoomDatabase = PhoneRoomDatabase.getDatabase(application);
        //repository uses DAO to query DB
        mPhoneDao = phoneRoomDatabase.phoneDao();
        mAllPhones = mPhoneDao.getAlphabetizedPhones();    //getting all phones from DAO
    }

    //getting all phones
    LiveData<List<Phone>> getAllPhones()
    {
        return mAllPhones;
    }

    //deleting all phones
    void deleteAll()
    {
        PhoneRoomDatabase.databaseWriteExecutor.execute(() -> mPhoneDao.deleteAll());
    }

    void insertDefault()
    {
        PhoneRoomDatabase.databaseWriteExecutor.execute(() -> {
            mPhoneDao.insert(new Phone("Google", "Nexus 5", 19, "https://www.lg.com/us/cell-phones/lg-D820-Sprint-Black-nexus-5"));
            mPhoneDao.insert(new Phone("Huawei", "Mate 10 Lite", 24, "https://en.wikipedia.org/wiki/Huawei_Mate_10"));
        });
    }

    void insert(Phone p)
    {
        PhoneRoomDatabase.databaseWriteExecutor.execute(() -> {
            mPhoneDao.insert(p);
        });
    }

    void delete(Phone p)
    {
        PhoneRoomDatabase.databaseWriteExecutor.execute(() -> {
            mPhoneDao.delete(p);
        });
    }

    void update(Phone p)
    {
        PhoneRoomDatabase.databaseWriteExecutor.execute(() -> {
            mPhoneDao.update(p);
        });
    }
}
