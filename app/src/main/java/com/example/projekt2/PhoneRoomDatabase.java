package com.example.projekt2;

//annotation defines the classes on which DB tables are based (Entities array),
//the DB version (to launch a correct migration),
//exportSchema = false suppresses the warnings during project building

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Phone.class}, version = 2, exportSchema = false)
public abstract class PhoneRoomDatabase extends RoomDatabase {

    //abstract method returning DAO
    public abstract PhoneDao phoneDao();

    //singleton implementation
    private static volatile PhoneRoomDatabase INSTANCE;

    static PhoneRoomDatabase getDatabase(final Context context)
    {
        //we create a new object only if doesn't exist already
        if(INSTANCE == null)
        {
            synchronized (PhoneRoomDatabase.class)
            {
                if(INSTANCE == null)
                {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            PhoneRoomDatabase.class, "phonedb")
                            //setting an object handling DB events
                            .addCallback(sRoomDatabaseCallback)
                            //a simple migration - deleting and recreating DB
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    //a helper service to fulfill tasks in a separate thread
    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    //object responsible for handling callbacks related to DB events eg. onCreate, onOpen
    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback()
    {
        //ran if DB doesn't exist (first launch)
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db)
        {
            super.onCreate(db);

            //running in a separate thread
            //the parameter of execute() is an object which implements Runnable interface
            //can be replaced with lambda
            databaseWriteExecutor.execute(() -> {
                PhoneDao dao = INSTANCE.phoneDao();
                //creating Phone objects and adding them to DB
                //with insert() DAO method
                //here we can preset default content of the database
                dao.insert(new Phone("Google", "Nexus 5", 19, "https://www.lg.com/us/cell-phones/lg-D820-Sprint-Black-nexus-5"));
                dao.insert(new Phone("Huawei", "Mate 10 Lite", 24, "https://en.wikipedia.org/wiki/Huawei_Mate_10"));
            });
        }
    };

}
