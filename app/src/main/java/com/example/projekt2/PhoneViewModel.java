package com.example.projekt2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class PhoneViewModel extends AndroidViewModel {

    private final PhoneRepository mRepository;
    private final LiveData<List<Phone>> mAllPhones;

    public PhoneViewModel(@NonNull Application application) {
        super(application);
        mRepository = new PhoneRepository(application);
        mAllPhones = mRepository.getAllPhones();    //getting all phones from the repository
    }

    public LiveData<List<Phone>> getAllPhones()
    {
        return mAllPhones;
    }

    public void deleteAll()
    {
        mRepository.deleteAll();
    }

    public void insertDefault() {mRepository.insertDefault();}

    public void insert(@NonNull Phone p) {mRepository.insert(p);}

    public void delete(@NonNull Phone p) {mRepository.delete(p);}

    public void update(@NonNull Phone p) {mRepository.update(p);}
}
