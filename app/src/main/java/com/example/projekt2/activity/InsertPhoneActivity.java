package com.example.projekt2.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.projekt2.R;

import java.util.HashMap;

public class InsertPhoneActivity extends AppCompatActivity {

    private EditText manufacturerInput, modelInput, versionInput, websiteInput;
    private HashMap<EditText, Boolean> correctnessFlags;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //getting request code from intent, default value is 1
        int requestCode = getIntent().getIntExtra("requestCode",
                MainActivity.REQUEST_CODE_INSERT);
        //getting references to layout elements
        manufacturerInput = findViewById(R.id.insertManufacturerInput);
        modelInput = findViewById(R.id.insertModelInput);
        versionInput = findViewById(R.id.insertVersionInput);
        websiteInput = findViewById(R.id.insertWebsiteInput);

        //if request code is 2 (update), fill the EditText views with initial data
        if(requestCode == MainActivity.REQUEST_CODE_UPDATE)
        {
            manufacturerInput.setText(getIntent().getStringExtra("manufacturer"));
            modelInput.setText(getIntent().getStringExtra("model"));
            versionInput.setText(String.valueOf(getIntent().getIntExtra("version", 0)));
            websiteInput.setText(getIntent().getStringExtra("website"));
        }

        Button website = findViewById(R.id.websiteButton);
        Button cancel = findViewById(R.id.cancelButton);
        Button save = findViewById(R.id.saveButton);

        //setting button listeners

        website.setOnClickListener(v -> {
            String address = websiteInput.getText().toString();
            if(address.startsWith("http://") || address.startsWith("https://"))
            {
                Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse(address));
                startActivity(browserIntent);
            }
            else
                Toast.makeText(getApplicationContext(), "Podano niepoprawny adres!", Toast.LENGTH_SHORT).show();
        });

        cancel.setOnClickListener(v -> {
            setResult(RESULT_CANCELED);
            finish();
        });

        save.setOnClickListener(v -> {
            //asserting all flags are set to true
            //if a flag set to false is found, a Toast is shown and function returns
            //without finishing activity
            for(Boolean f : correctnessFlags.values())
            {
                if(!f) {
                    Toast.makeText(getApplicationContext(), R.string.error_input_empty, Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            //saving data to bundle
            Bundle data = new Bundle();
            data.putString("manufacturer", manufacturerInput.getText().toString());
            data.putString("model", modelInput.getText().toString());
            data.putInt("system", Integer.parseInt(versionInput.getText().toString()));
            data.putString("website", websiteInput.getText().toString());
            //if doing an update (request code 2), add phone_id to the bundle
            //to identify the phone to be updated
            data.putLong("phoneId", getIntent().getLongExtra("phoneId", 0));
            //preparing return intent
            Intent intent = new Intent();
            intent.putExtras(data);
            setResult(RESULT_OK, intent);
            //finishing activity
            finish();
        });

        //setting input fields validation
        setupValidation();
        //setting correctness flags initial values
        setupCorrectnessFlags(requestCode);
    }


    //HELPER METHODS

    private void setupValidation()
    {
        manufacturerInput.setOnFocusChangeListener(
                (v, hasFocus) -> {
                    if(!hasFocus)
                    {
                        validateInput(manufacturerInput);
                    }
                }
        );
        modelInput.setOnFocusChangeListener(
                (v, hasFocus) -> {
                    if(!hasFocus)
                    {
                        validateInput(modelInput);
                    }
                }
        );
        versionInput.setOnFocusChangeListener(
                (v, hasFocus) -> {
                    if(!hasFocus)
                    {
                        validateInput(versionInput);
                    }
                }
        );
        //TODO: proper website validation (regex?)
        websiteInput.setOnFocusChangeListener(
                (v, hasFocus) -> {
                    if(!hasFocus)
                    {
                        validateInput(websiteInput);
                    }
                }
        );
        manufacturerInput.addTextChangedListener(createTextWatcher(manufacturerInput));
        modelInput.addTextChangedListener(createTextWatcher(modelInput));
        versionInput.addTextChangedListener(createTextWatcher(versionInput));
        websiteInput.addTextChangedListener(createTextWatcher(websiteInput));
    }

    private void setupCorrectnessFlags(int requestCode)
    {
        this.correctnessFlags = new HashMap<>();
        switch(requestCode)
        {
            case 1:
                //initially setting correctness flags to false
                this.correctnessFlags.put(manufacturerInput, Boolean.FALSE);
                this.correctnessFlags.put(modelInput, Boolean.FALSE);
                this.correctnessFlags.put(versionInput, Boolean.FALSE);
                this.correctnessFlags.put(websiteInput, Boolean.FALSE);
                break;
            case 2:
                //initially setting correctness flags to true
                this.correctnessFlags.put(manufacturerInput, Boolean.TRUE);
                this.correctnessFlags.put(modelInput, Boolean.TRUE);
                this.correctnessFlags.put(versionInput, Boolean.TRUE);
                this.correctnessFlags.put(websiteInput, Boolean.TRUE);
                break;
        }
    }

    //returns false if parameter EditText is empty
    private void validateInput(EditText input)
    {
        if (input.getText().toString().isEmpty()) {
            input.setError(getString(R.string.error_input_empty));
            correctnessFlags.put(input, Boolean.FALSE);
        } else correctnessFlags.put(input, Boolean.TRUE);
    }

    private TextWatcher createTextWatcher(EditText input)
    {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                validateInput(input);
            }
        };
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString("manufacturer", manufacturerInput.getText().toString());
        outState.putString("model", modelInput.getText().toString());
        outState.putInt("version", Integer.parseInt(versionInput.getText().toString()));
        outState.putString("website", websiteInput.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        manufacturerInput.setText(savedInstanceState.getString("manufacturer"));
        modelInput.setText(savedInstanceState.getString("model"));
        versionInput.setText(String.valueOf(savedInstanceState.getInt("version")));
        websiteInput.setText(savedInstanceState.getString("website"));
    }
}
