package com.example.projekt2.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.projekt2.OnItemClickListener;
import com.example.projekt2.Phone;
import com.example.projekt2.PhoneListAdapter;
import com.example.projekt2.PhoneViewModel;
import com.example.projekt2.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity implements OnItemClickListener {

    private PhoneViewModel mPhoneViewModel;
    private PhoneListAdapter mAdapter;
    private ItemTouchHelper mItemTouchHelper;

    public static final int REQUEST_CODE_INSERT = 1;
    public static final int REQUEST_CODE_UPDATE = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //setting adapter on the list
        RecyclerView phoneList = findViewById(R.id.phoneList);
        mAdapter = new PhoneListAdapter(this);
        phoneList.setAdapter(mAdapter);
        //setting list layout
        phoneList.setLayoutManager(new LinearLayoutManager(this));

        //reading view model from the provider
        mPhoneViewModel = new ViewModelProvider(this).get(PhoneViewModel.class);

        //when data set in LiveData object in view model change,
        //a method will be called to set the updated list in the adapter
        mPhoneViewModel.getAllPhones().observe(this, phones -> mAdapter.setPhoneList(phones));

        //creating ItemTouchHelper and binding it to RecyclerView
        mItemTouchHelper = new ItemTouchHelper
                (new ItemTouchHelper.SimpleCallback
                        (0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView,
                                  @NonNull RecyclerView.ViewHolder viewHolder,
                                  @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                //getting a phone object from adapter to be passed to DAO method for deletion
                Phone p = mAdapter.getPhone(viewHolder.getAdapterPosition());
                //deleting phone from DB
                mPhoneViewModel.delete(p);
                //showing toast with feedback
                Toast.makeText(getApplicationContext(), "Usuwam telefon: " + p.toString(), Toast.LENGTH_LONG)
                        .show();
            }
        });
        mItemTouchHelper.attachToRecyclerView(phoneList);
        mAdapter.setOnItemClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        //getting reference to the FAB
        FloatingActionButton addButton = findViewById(R.id.addButton);
        //setting listener for the button
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //creating intent
                Intent intent = new Intent(MainActivity.this, InsertPhoneActivity.class);
                intent.putExtra("requestCode", REQUEST_CODE_INSERT);
                //starting activity
                startActivityForResult(intent, REQUEST_CODE_INSERT);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_INSERT) {
                //check if data is present
                assert data != null;
                //retrieving data from intent bundle
                Bundle result = data.getExtras();
                String manufacturer = result.getString("manufacturer");
                String model = result.getString("model");
                Integer system = result.getInt("system");
                String website = result.getString("website");
                //TODO: backend validation
                //saving data to DB
                Phone p = new Phone(manufacturer, model, system, website);
                Toast.makeText(this, p.toString(), Toast.LENGTH_SHORT).show();
                mPhoneViewModel.insert(p);
            }
            if(requestCode == REQUEST_CODE_UPDATE)
            {
                //check if data is present
                assert data != null;
                //retrieving data from intent bundle
                Bundle result = data.getExtras();
                String manufacturer = result.getString("manufacturer");
                String model = result.getString("model");
                Integer system = result.getInt("system");
                String website = result.getString("website");
                long phoneId = result.getLong("phoneId");
                //TODO: backend validation
                //saving data to DB
                Phone p = new Phone(phoneId, manufacturer, model, system, website);
                Toast.makeText(this, "Zmieniam telefon: " + p.toString(), Toast.LENGTH_SHORT).show();
                mPhoneViewModel.update(p);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch(id)
        {
            case R.id.menuClearAll:
                mPhoneViewModel.deleteAll();
                return true;

            case R.id.menuInsertDefault:
                mPhoneViewModel.insertDefault();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //this is the implementation o custom OnItemClickListener interface method
    //it handles clicks on RecyclerView list items, calling InsertPhoneActivity with request code 2
    //passing current data of clicked phone to the activity
    @Override
    public void onClick(View v, int position) {
        //debug logcat msg
        Log.println(Log.INFO, "Main", "PhoneList item clicked: position " + position);
        //getting proper phone from adapter
        Phone p = mAdapter.getPhone(position);
        //creating intent
        Intent intent = new Intent(MainActivity.this, InsertPhoneActivity.class);
        //preparing data for update
        Bundle formData = new Bundle();
        formData.putLong("phoneId", p.getPhone_id());
        formData.putString("manufacturer", p.getManufacturer());
        formData.putString("model", p.getModel());
        formData.putInt("version", p.getSystem());
        formData.putString("website", p.getWebsite());
        //attaching data to intent
        intent.putExtras(formData);
        intent.putExtra("requestCode", REQUEST_CODE_UPDATE);
        //starting activity
        startActivityForResult(intent, REQUEST_CODE_UPDATE);
    }
}